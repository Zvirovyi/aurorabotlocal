#include <AuroraBot/AuroraBot.h>

namespace AuroraBot {

    void AuroraBot::onReady(SleepyDiscord::Ready readyData) {
        std::vector<std::string> servers;
        for (const SleepyDiscord::Server &server : std::vector<SleepyDiscord::Server>(getServers())) {
            servers.emplace_back(server.ID.string());
        }
        db->remove_all<DB::Server>(sqlite_orm::where(sqlite_orm::not_in(&DB::Server::ID, servers)));
    }

}