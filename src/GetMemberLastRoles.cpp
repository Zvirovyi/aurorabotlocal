#include <AuroraBot/AuroraBot.h>

namespace AuroraBot {

    std::vector<DB::MemberLastRole> AuroraBot::GetMemberLastRoles(std::string_view serverID, std::string_view userID) {
        return db->get_all<DB::MemberLastRole>(sqlite_orm::where(sqlite_orm::bitwise_and(sqlite_orm::is_equal(&DB::MemberLastRole::ServerID, (std::string)serverID), sqlite_orm::is_equal(&DB::MemberLastRole::UserID, (std::string)userID))));
    }

}