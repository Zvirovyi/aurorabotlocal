#include <AuroraBot/AuroraBot.h>

namespace AuroraBot {

    std::vector<std::string> AuroraBot::SplitCommand(std::string_view command) {
        std::vector<std::string> res;
        std::string part;
        for (char ch: command) {
            if (ch != ' ') {
                part += ch;
            } else if (!part.empty()) {
                res.push_back(part);
                part.clear();
            }
        }
        if (!part.empty()) {
            res.push_back(part);
        }
        return res;
    }

}