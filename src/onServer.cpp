#include <AuroraBot/AuroraBot.h>

namespace AuroraBot {

    void AuroraBot::onServer (SleepyDiscord::Server server) {
        std::optional<DB::Server> dbServer = db->get_optional<DB::Server>(server.ID.string());
        //TODO remove hardcode
        std::vector<SleepyDiscord::ServerMember> serverMembers = listMembers(server.ID, 1000).vector();
        if (!dbServer.has_value()) {
            db->replace(DB::Server{server.ID.string(), std::nullopt, std::nullopt});
        } /*else if (dbServer.value().AutoRoleID.has_value()) {
            for (const SleepyDiscord::ServerMember &member : serverMembers) {
                if (member.roles.empty()) {
                    addRole(server.ID, member.ID, dbServer.value().AutoRoleID.value());
                }
            }
        }*/
        for (const SleepyDiscord::ServerMember &member : serverMembers) {
            std::vector<DB::MemberLastRole> memberLastRoles = GetMemberLastRoles(server.ID.string(), member.ID.string());
            if (memberLastRoles.empty() && member.roles.empty() && dbServer.has_value() && dbServer.value().AutoRoleID.has_value()) {
                addRole(server.ID, member.ID, dbServer.value().AutoRoleID.value());
                db->replace(DB::MemberLastRole{server.ID.string(), member.ID.string(), dbServer.value().AutoRoleID.value()});
            } else {
                std::vector<std::string> memberLastRolesToAdd;
                for (const SleepyDiscord::Snowflake<SleepyDiscord::Role> &memberRole : member.roles) {
                    if (std::find_if(memberLastRoles.begin(), memberLastRoles.end(), [&](const DB::MemberLastRole &memberLastRole){
                        return memberLastRole.RoleID == memberRole.string();
                    }) == memberLastRoles.end()) {
                        memberLastRolesToAdd.emplace_back(memberRole.string());
                    }
                }
                std::vector<std::string> memberLastRolesToDelete;
                for (const DB::MemberLastRole &memberLastRole : memberLastRoles) {
                    if (std::find_if(member.roles.begin(), member.roles.end(), [&](const SleepyDiscord::Snowflake<SleepyDiscord::Role> &memberRole){
                        return memberRole.string() == memberLastRole.RoleID;
                    }) == member.roles.end()) {
                        memberLastRolesToDelete.emplace_back(memberLastRole.RoleID);
                    }
                }
                db->remove_all<DB::MemberLastRole>(sqlite_orm::where(sqlite_orm::bitwise_and(sqlite_orm::bitwise_and(sqlite_orm::is_equal(&DB::MemberLastRole::ServerID, server.ID.string()), sqlite_orm::is_equal(&DB::MemberLastRole::UserID, member.ID.string())), sqlite_orm::in(&DB::MemberLastRole::RoleID, memberLastRolesToDelete))));
                for (const std::string &memberLastRoleToAdd : memberLastRolesToAdd) {
                    db->replace(DB::MemberLastRole{server.ID.string(), member.ID.string(), memberLastRoleToAdd});
                }
            }
        }
    }

}