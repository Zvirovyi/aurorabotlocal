#include <AuroraBot/AuroraBot.h>

namespace AuroraBot {

    void AuroraBot::onMember(SleepyDiscord::Snowflake<SleepyDiscord::Server> serverID, SleepyDiscord::ServerMember member) {
        std::vector<DB::MemberLastRole> memberLastRoles = GetMemberLastRoles(serverID.string(), member.ID.string());
        if (memberLastRoles.empty()) {
            DB::Server dbServer = db->get<DB::Server>(serverID.string());
            if (dbServer.AutoRoleID.has_value()) {
                addRole(serverID, member.ID, dbServer.AutoRoleID.value());
                db->replace(DB::MemberLastRole{serverID.string(), member.ID.string(), dbServer.AutoRoleID.value()});
            }
        } else {
            for (const DB::MemberLastRole &memberLastRole : memberLastRoles) {
                addRole(serverID, member.ID, memberLastRole.RoleID);
            }
        }
    }

}